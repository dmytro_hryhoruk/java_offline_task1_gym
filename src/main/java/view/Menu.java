package view;
import model.Gym;

import java.util.Scanner;
public enum Menu  {
    CHOOSE_EXERCISE("Find required machine"),QUIT("Exit");

    private String menuName;
    Menu() {
    }

    Menu(String name){
        this.menuName =name;
    }
    public String getMenuName() {
        return menuName;
    }
    public void showMenu(){
        for(Menu m:Menu.values()){
            System.out.println(m.ordinal()+1+". "+m.getMenuName());
        }
    }
    public Gym gym = new Gym();
    static Scanner src =new Scanner(System.in);
    public static void UserChoice(Gym gym){
        int userChoice =src.nextInt();
        switch (userChoice){
            case(1):
                int requiredTime = src.nextInt():
                int requiredLevel =src.nextInt();
                gym.findRequiredMachine(gym.getMachines(),requiredTime,requiredLevel);
                break;
            case(2):
                System.exit(0);
            default:
                Menu.UserChoice();
        }
    }
}
