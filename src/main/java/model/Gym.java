package model;

import java.util.ArrayList;

public class Gym implements WorkOut{
    private static ArrayList<ExerciseMachine> machines = new ArrayList<ExerciseMachine>();
    public  ArrayList<ExerciseMachine> getMachines() {
        return machines;
    }
    public static void addMachine(ExerciseMachine machine) {
        this.machines.add(machine);
    }


    public String findRequiredMachine(ArrayList<ExerciseMachine> machines,int requiredTime,int requiredLevel) {
        for(ExerciseMachine machine:machines){
            if(machine.getTime()<=requiredTime+5 || machine.getTime()>=requiredTime-5){
                if(machine.getHowHard()<=requiredLevel+2||machine.getHowHard()>=requiredLevel-2){
                    return machine.getName();
                }else{
                    return "There is no machine suitable for you according to the level yo've chosen";
                }
            }else{
               return "There is no machine suitable for you according to the time yo've chosen";
            }
        }
        return "There is no machine suitable for you, Try another one";
    }
}
