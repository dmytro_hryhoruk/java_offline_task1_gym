package model;

public abstract class ExerciseMachine extends Gym{
    private int time;
    private int howHard;
    private String name;


    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getHowHard() {
        return howHard;
    }

    public void setHowHard(int howHard) {
        this.howHard = howHard;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
