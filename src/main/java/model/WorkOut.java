package model;

import java.util.ArrayList;

public interface WorkOut {
    String findRequiredMachine(ArrayList<ExerciseMachine> machine,int requiredTime,int requiredLevel);
}
